package apps

/**
  * Created by mac010 on 2017/4/17.
  */

object FractionApp extends App {

  val frac = cc.Fraction(3,6)
  println(frac.reduce())

  val frac1=cc.Fraction(1,2)
  val frac2=cc.Fraction(1,3)

  val frac3=frac1.plus(frac2)
 //=frac1 plus frac2
    println(frac3)
}

